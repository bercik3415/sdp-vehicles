import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

enum Type {
    CAR,
    SHIP,
    PLANE,
    BICYCLE
}

public class Vehicles implements Comparable <Vehicles> {

    private int predkosc;
    private String producent;
    private Type type;
    private static final Logger logger = LogManager.getLogger(Vehicles.class);

    public Vehicles(int predkosc, String producent, Type type)
    {
        this.predkosc=predkosc;
        this.producent=producent;
        this.type=type;

    }
    public int getPredkosc()
    {return this.predkosc;}
    public String getProducent()
    {return this.producent;}
    public Type getType()
    {return this.type;}




    public static void main (String [] args)
    {

        boolean petla=true;
        Scanner scan= new Scanner(System.in);
        int settings;

        List<Vehicles> list = new ArrayList<>();
        list.add(new Vehicles(60, "BMW",Type.CAR));
        list.add(new Vehicles(80, "MERCEDES",Type.CAR));
        list.add(new Vehicles(20, "KROSS",Type.BICYCLE));
        list.add(new Vehicles(15, "ORBEA",Type.BICYCLE));
        list.add(new Vehicles(50, "ELLEN",Type.SHIP));
        list.add(new Vehicles(80, "MARINE",Type.SHIP));
        list.add(new Vehicles(800, "AREO",Type.PLANE));
        list.add(new Vehicles(900, "FIAT",Type.PLANE));

        Collections.sort(list);

        while(petla){
            try {
                logger.debug("Wybierz jedna z opcji:\n1. CAR\n2. BIKE\n3. SHIP\n4. PLANE\n5. ALL\n6. EXIT");
                settings = scan.nextInt();
                switch (settings) {
                    case 1:
                        for (Vehicles x : list)
                        {
                            if(x.getType().equals(Type.CAR))
                            {
                                logger.debug("Typ pojazdu "+ x.getType()+ " od producenta "+ x.getProducent()+
                                        " jest najszybszy z predkoscia " + x.getPredkosc());
                                break;
                            }
                        }
                        break;
                    case 2:
                        for (Vehicles x : list)
                        {
                            if(x.getType().equals(Type.BICYCLE))
                            {
                                logger.debug("Typ pojazdu "+ x.getType()+ " od producenta "+ x.getProducent()+
                                        " jest najszybszy z predkoscia " + x.getPredkosc());
                                break;
                            }
                        }
                        break;
                    case 3:
                        for (Vehicles x : list)
                        {
                            if(x.getType().equals(Type.SHIP))
                            {
                                logger.debug("Typ pojazdu "+ x.getType()+ " od producenta "+ x.getProducent()+
                                        " jest najszybszy z predkoscia " + x.getPredkosc());
                                break;
                            }
                        }
                        break;
                    case 4:
                        for (Vehicles x : list)
                        {
                            if(x.getType().equals(Type.PLANE))
                            {
                                logger.debug("Typ pojazdu "+ x.getType()+ " od producenta "+ x.getProducent()+
                                        " jest najszybszy z predkoscia " + x.getPredkosc());
                                break;
                            }
                        }
                        break;
                    case 5:
                        for (Vehicles x : list)
                        {
                            logger.debug("Typ pojazdu "+ x.getType()+ " od producenta "+ x.getProducent()+
                                    " jest najszybszy z predkoscia " + x.getPredkosc());
                        }
                        break;
                    case 6:
                        logger.debug("EXIT");
                        petla = false;
                        break;
                    default:
                        logger.debug("Zly numer opcji");
                }
            } catch (java.util.InputMismatchException e) {
                logger.debug("Błędny typ danych wejsciowych");
                scan.next();
            }
        }


    }

    @Override
    public int compareTo(Vehicles o) {
        int result=o.predkosc-this.predkosc;
        return result;

    }
}
